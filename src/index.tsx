// -*- mode: javascript -*-


/*
 * imports
 */
import 'bootstrap/dist/css/bootstrap.css'
import {
    Action, createStore, State, rotateTile, selectTile, Tools,
    listenMouse, TileCoord, setTool, Map, flipTile, Tile, ITileInfo,
    paintTiles
} from 'hexkit'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import * as  jsonFormat from 'json-format'
import { Button, Row, Alert, Container, Col } from 'reactstrap'
import { connect, Provider } from 'react-redux'
import { Dispatch } from 'redux'
import * as _ from 'lodash'


/*
 * declarations
 */
let paintSize = 1
let store = null


/*
 * utility functions
 */
const indexToCoordinate = (index: number, map: Map): TileCoord =>
      index > -1
      ? {
          x: index % map.width,
          y: Math.floor(index / map.width)
      }
      : null
// const coordinateToIndex = (coord: TileCoord, map: Map): number => {}
let uncheckAllButtons = function() {
    for (let i = 0; i <= 3; i++) {
        document.getElementById("size" + i).style.color = "secondary"
    }
}

let setSize = (size: number): number => {
    uncheckAllButtons()
    this.color = 'primary'
    return (paintSize = size)
}
let flatten = function(tilesetUri: string | string[]): string {
    if (typeof tilesetUri === "string") {
        return tilesetUri
    } else {
        return tilesetUri[Math.floor(Math.random()*tilesetUri.length)]
    }
}

/*
 * interfaces
 */
interface In {
    appState: State
}

interface Out {
    startInspecting: () => void
    selectTile: (coordinate: TileCoord) => void
    rotateTile: (coord: TileCoord) => void
    flipTile: (coord: TileCoord) => void
}


/*
 * main plugin loop
 */
let PluginComponent = ({ appState, startInspecting }: In & Out) => {
    let painting = appState.tool == Tools.Paint
    let inspecting = appState.tool == Tools.Info
    let tile: Tile
    let coordinate: TileCoord
    let paintThese: { coord: TileCoord, layer: number, uri: string }[] = new Array()

    if (inspecting && appState && appState.layer != null && appState.tilesetUri != null) {
        let index = _.findIndex(appState.map.infoLayer, info => info.selected)
        coordinate = indexToCoordinate(index, appState.map)
        tile = appState.map.layers[0].tiles[index]

        // paint only when the selected tile is blank
        if (coordinate
            && tile && tile.source.startsWith("Blank")) {
            // iterate vertically and horizontally
            for (let i = Math.max(0, coordinate.x - paintSize);
                 i < Math.min(appState.map.width, coordinate.x + paintSize + 1);
                 i++) {
                for (let j = Math.max(0, coordinate.y - paintSize);
                     j < Math.min(appState.map.height, coordinate.y + paintSize + 1);
                    j++) {
                    paintThese.push({
                        coord: {x: i, y: j},
                        layer: appState.layer,
                        uri: flatten(appState.tilesetUri)
                    })
                }
            }

            // if we found any valid positions to paint, paint them.
            if (paintThese.length >= 0 && store) {
                store.dispatch(paintTiles(paintThese))
            }
        }
    }

    return <Container fluid={true} style={{ paddingTop: 10, paddingBottom: 10 }}>
        <Row>
          <Col>
            Brush Size:
          </Col>
          <Col>
            <Button
              id="size0"
              color={paintSize == 0 ? 'primary' : 'secondary'}
              onClick={() => setSize(0)}>1</Button>
          </Col>
          <Col>
            <Button
              id="size1"
              color={paintSize == 1 ? 'primary' : 'secondary'}
              onClick={() => setSize(1)}>3</Button>
          </Col>
          <Col>
            <Button
              id="size2"
              color={paintSize == 2 ? 'primary' : 'secondary'}
              onClick={() => setSize(2)}>5</Button>
          </Col>
          <Col>
            <Button
              id="size3"
              color={paintSize == 3 ? 'primary' : 'secondary'}
              onClick={() => setSize(3)}>7</Button>
          </Col>
        </Row>
        </Container>
}


/*
 * plumbing to connect plugin to events
 */
let Plugin = connect<In, Out>(
    (state: State) => ({
        appState: state
    }),
    (dispatch: Dispatch<State>) => ({
        startInspecting: () => dispatch(setTool(Tools.Paint)),
        // functions we need for the type signature that we don't much care about.
        selectTile: (coord: TileCoord) => dispatch(selectTile(coord)),
        rotateTile: (coord: TileCoord) => dispatch(rotateTile(coord)),
        flipTile: (coord: TileCoord) => dispatch(flipTile(coord))
    })
)(PluginComponent)


/*
 * main
 */
window.onload = () => {
    store = createStore()

    ReactDOM.render(
            <Provider store={store}><Plugin /></Provider>,
        document.getElementById('plugin'))
}
