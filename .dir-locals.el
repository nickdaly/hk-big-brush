(("src/"
  (nil .
       ((fill-column . 80)
        (eval add-hook
	      'after-save-hook
	      '(lambda ()
		 (compile "make -kC .. clean all"))
	      ;; it would be nice to use: echo "make -k" | at now + 1 minute
	      ;; but that doesn't spawn a compile buffer.  the alternative is:
	      ;; (compile "sleep \"$((60 - (`date +%s` % 60)))\" && make -kC .."))
              ;; but that doesn't really do what I want either, because
              ;; compilation takes a really long time, so the compiling code
              ;; desyncs with what I'm working on.
	      nil t)))))
