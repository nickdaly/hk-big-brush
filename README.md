# Big Brush

This Hex Kit plugin paints multiple groups of tiles at once.

Download it from:

* https://nickdaly.gitlab.io/hk-big-brush/big-brush.zip

Just drop it in your `Documents/Hex Kit/Plugins` folder and restart Hex Kit.

# Development

Start with these references:

* https://github.com/rossimo/hexkit-sample

* https://rossimo.github.io/hexkit-sample/

* https://rossimo.github.io/hexkit-sample/modules/_index_.html

* https://rossimo.github.io/hexkit-sample/modules/_types_.html

To build it, try the NPM magic, or run `make`.
